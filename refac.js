after
function Person(firstName, lastName, gender, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.age = age;
  }
  
  Person.prototype = {
    getFullName: function() { return this.firstName + ' ' + this.lastName; },
    isMale: function() { return this.gender == 'Male'; },
    isFemale: function() { return this.gender == 'Female'; }
  };
  
  var amanda = new Person('Amanda', 'Smith', "Female", 42);
  var john = new Person('John', 'Doe', 'Male', 72);
  
  alert(amanda.getFullName());
  alert(john.isMale());
///before
  function getFullName(firstName, lastName) {
    return firstName + ' ' + lastName;
  }
  
  function isMale(gender) {
    return gender == 'Male';
  }
  
  function isFemale(gender) {
    return gender == 'Female';
  }
  
  var amandaFirstName = 'Amanda';
  var amandaLastName = 'Smith';
  var amandaGender = 'Female';
  var amandaAge = 42;
  var johnFirstName = 'John';
  var johnLastName = 'Doe';
  var johnGender = 'Male';
  var johnAge = 72;
  
  alert(getFullName(amandaFirstName, amandaLastName));
  alert(isMale(johnGender));